<?php
	require_once("./Predis/Autoloader.class.php");
	class createWord{
		public $redisObj;
		// public static $wordsArr = array();

		public function __construct(){
			
			\Predis\Autoloader::register(); 

	        $this -> redisObj = new \Predis\Client(array(
			    'scheme'   => 'tcp',
			    'host'     => '127.0.0.1',
			    'port'     => 6379,
			    'database' => 15,
			    'password' => 'xxxxxx'
			));
		}

		public function word($perLine){
			
			preg_match_all('/[a-z]+/i',$perLine,$matches);

			if($matches[0]){
				foreach ($matches[0] as $key => $value) {
					$word = strtolower($value);
					if($this -> redisObj->exists($word)){
						$this -> redisObj->incr($word);

					}else{

						$this -> redisObj->set($word, 1);
					}
					// if(!in_array($value, self::$wordsArr)){
					// 	self::$wordsArr[] = $value;

					// }
					
				}

			}
		}

		/** 
		 * 返回文件从X行到Y行的内容(支持php5、php4)  
		 * @param string $filename 文件名
		 * @param int $startLine 开始的行数
		 * @param int $endLine 结束的行数
		 * @return string
		 */
		public function getFileLines($filename, $startLine = 1, $endLine=50, $method='rb') {
		    $content = array();
		    $count = $endLine - $startLine;  
		    // 判断php版本（因为要用到SplFileObject，PHP>=5.1.0）
		    if(version_compare(PHP_VERSION, '5.1.0', '>=')){
		        $fp = new SplFileObject($filename, $method);
		        $fp->seek($startLine-1);// 转到第N行, seek方法参数从0开始计数
		        for($i = 0; $i <= $count; ++$i) {
		            $lineContent=$fp->current();// current()获取当前行内容
		            $this -> word($lineContent);
		            $fp->next();// 下一行
		        }
		    }else{//PHP<5.1
		        // $fp = fopen($filename, $method);
		        // if(!$fp) return 'error:can not read file';
		        // for ($i=1;$i<$startLine;++$i) {// 跳过前$startLine行
		        //     fgets($fp);
		        // }
		        // for($i;$i<=$endLine;++$i){
		        //     $content[]=fgets($fp);// 读取文件行内容
		        // }
		        // fclose($fp);
		    }
		    //return array_filter($content); // array_filter过滤：false,null,''
		} 
		/** 
		 * 高效率计算文件行数
		 */
		public function countLine($file)
		{
		  $fp = fopen($file, "rb");
		  $i = 0;
		  while(!feof($fp)) {        
		    if($data = fread($fp, 1024*1024*2)){
		      $num = substr_count($data, PHP_EOL);
		      $i += $num;
		    }
		  }
		  fclose($fp);
		  return $i;
		}   
	}
	set_time_limit(0);
	$fileName = './big.txt';
	$obj = new createWord();
	$lines = $obj -> countLine($fileName);
	$obj -> getFileLines($fileName,1,$lines);
	
	// print_r($obj::$wordsArr);
	