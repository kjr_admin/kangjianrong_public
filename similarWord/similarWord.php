<?php
	require_once("./Predis/Autoloader.class.php");
	class similarWord{
		public $alphabet = 'abcdefghijklmnopqrstuvwxyz';

		public $redisObj;
		// public static $wordsArr = array();

		public function __construct(){
			
			\Predis\Autoloader::register();  

	        $this -> redisObj = new \Predis\Client(array(
			    'scheme'   => 'tcp',
			    'host'     => '127.0.0.1',
			    'port'     => 6379,
			    'database' => 15,
			    'password' => 'xxxxxx'
			));

		}


		public function getSimilarWord_1($word){
			if(trim($word)){
				$word = strtolower($word);

				for($i=0; $i<strlen($word);$i++){
					$deletes[] = substr_replace($word,'',$i,1);
				}
				for($i=0; $i<strlen($word)-1;$i++){
					$transposes[] = substr_replace($word,$word[$i+1],$i,1);
				}
				for($i=0; $i<strlen($word);$i++){
					for($j=0;$j<strlen($this -> alphabet);$j++){
						$replaces[] = substr_replace($word,$this -> alphabet[$j],$i,1);
					}
				}
				for($j=0;$j<strlen($this -> alphabet);$j++){
					$inserts[] = $this -> alphabet[$j] . $word;
				}
				
				for($i=0; $i<strlen($word)-1;$i++){
					for($j=0;$j<strlen($this -> alphabet);$j++){
						$inserts[] = substr($word,0,$i+1) . $this -> alphabet[$j] . substr($word,-(strlen($word)-$i-1));
					}

				}
				
				for($j=0;$j<strlen($this -> alphabet);$j++){
					$inserts[] = $word . $this -> alphabet[$j];
				}
				$rs = array_unique(array_merge($deletes, $transposes, $replaces, $inserts));
				echo count($rs);
				$realWords = $this -> getRealWord($rs);
				print_r($realWords);
				return $realWords;
				
			}
		}

		// public function getSimilarWord_2($word){
		// 	$wordRs_1 = $this -> getSimilarWord_1($word);
		// 	foreach ($wordRs_1 as $key => $value) {
		// 		$wordRs_2[] = $this -> getSimilarWord_1($value);
				
		// 	}
		// 	print_r($wordRs_1);
		// 	print_r($wordRs_2);
		// }
		public function getRealWord($wordArr){
			foreach ($wordArr as $key => $word) {
				if($this -> redisObj->exists($word)){
					$realWords[] = $word;
				}
			}
			return $realWords;
		}

	}

	$obj = new similarWord();
	$obj -> getSimilarWord_1('hello');
